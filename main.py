from Secret import Secret
import random


def gen_num():
    num = random.randint(1, 100)
    hints = []
    divisible_by_2 = num % 2
    if divisible_by_2 != 0:
        hints.append("It is an odd number.")
    if num < 100:
        hints.append("It is less than 100.")
    if num > 50:
        hints.append("It is greater than 50")
    else:
        hints.append("It is less than 50")

    generated = Secret(num, hints)
    return generated


def run_game():
    secret_number = gen_num()
    # user has 3 guesses, start at 0
    guess_limit = 3
    guess_count = 0
    # user still has the option to guess some more
    can_guess = True
    user_answer = ""

    print("Guess the random number between 0 and 100 using the hints provided \n")

    try:
        while guess_count < guess_limit and can_guess:
            print(secret_number.hints[guess_count] + "\n")
            user_answer = float(input("What is the number? : "))
            guess_count += 1
            if user_answer == secret_number.answer:
                can_guess = False
    except ValueError:
        print("Invalid user input")

    if user_answer != secret_number.answer:
        print("You're out of tries!")
    else:
        print("You are right!")


run_game()
